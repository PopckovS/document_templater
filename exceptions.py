"""Модуль содержит исключения возникающие в работе генерации документации"""


class NotInheritedMethodTypeError(Exception):
    def __init__(self, text):
        self.txt = text


class NotAllowedDocumentationTypeError(Exception):
    def __init__(self, text):
        self.txt = text

