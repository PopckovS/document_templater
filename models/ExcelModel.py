from models.BaseExcelModels import BaseSheetModel
from utils.utils import get_title_from_registration


class ListSheet(BaseSheetModel):
    """Отражает модель страницы для Excel файла `Перечень` типа документов `gs` и `ngs`"""

    def create_content(self, context):
        """Страница: Перечень"""
        csv_meta = self.meta_source['csv']
        csv_dict = self.source['csv']

        items, data = [], {}

        for row in csv_dict:
            for key, val in csv_meta.items():
                data[key] = row[val]

            data = get_title_from_registration(data)

            data['area'] = int(data['area'])
            items.append(data)
            data = {}

        return items


class SummaryStatementSheet(BaseSheetModel):
    """Страница: Сводная ведомость"""
    def create_content(self):
        """Возвращает словарь с данными для вставки в шаблон страницы"""
        csv_meta = self.meta_source['csv']
        csv_dict = self.source['csv']
        excel_meta = self.meta_source['excel']
        excel_dict = self.source['excel']

        items, data = [], {}

        for csv_row in csv_dict:
            for ex_row in excel_dict:
                # если кадастровый номер из разных источников совпадает
                if csv_row['Кадастровый номер'] == ex_row['Кадастровый номер ']:
                    for key, val in csv_meta.items():
                        data[key] = csv_row[val]
                    for key, val in excel_meta.items():
                        data[key] = ex_row[val]

            data['area'] = int(data['area'])
            items.append(data)
            data = {}

        return items


class RemovalSheet(BaseSheetModel):
    """Отражает модель страницы для Excel файла `Перечень` типа документов `removal`"""

    def create_context(self):
        """Возвращает словарь с данными для вставки в шаблон страницы"""
        csv_meta = self.meta_source['csv']
        csv_dict = self.source['csv']
        excel_meta = self.meta_source['excel']
        excel_dict = self.source['excel']

        document_info = 'Выписка из Единого государственного реестра недвижимости об объекте недвижимости  от {data} № {registration_number}'

        items, data = [], {}

        for csv_row in csv_dict:
            for ex_row in excel_dict:
                # если кадастровый номер из разных источников совпадает
                if csv_row['Кадастровый номер'] == ex_row['Кадастровый номер существующего ЗУ/КК']:
                    for key, val in csv_meta.items():
                        data[key] = csv_row[val]
                    for key, val in excel_meta.items():
                        data[key] = ex_row[val]

                    # константа для заполнения
                    data['naming'] = 'Земельный участок'
                    data['document_info'] = document_info.format(
                        data=data['date'],
                        registration_number=data['reg_number'],
                    )
                    data = get_title_from_registration(data)

            items.append(data)
            data = {}

        return items
