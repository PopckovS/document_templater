import copy
from xlsxtpl.writerx import BookWriter
from names import DOCUMENT_TYPE, EXCEL_TEMPLATE
from exceptions import NotAllowedDocumentationTypeError, NotInheritedMethodTypeError
import logging

logger = logging.getLogger(__name__)


class BaseSheetModel:
    """Модель базовой страницы"""

    name = None # название страницы
    index = None # тип документации к которому принадлежит страница
    meta_source = {} # мета информация о входных данных, что за поля
    source = {} # входные данные для обработки
    data = {} # результат обработки, данные для вставки в шаблон страницы

    def __init__(self, type, name, index):
        if type not in DOCUMENT_TYPE:
            raise NotAllowedDocumentationTypeError('Не разрешенный тип документации `%s`' % type)
        self.meta_source = copy.deepcopy(EXCEL_TEMPLATE[type][name])
        self.index = index
        self.type = type
        self.name = name
        self.data = {
            'sheet_name': self.name,
            'tpl_index': self.index,
        }

    def set_data(self, **kwargs):
        """Загружаем в страницу данные для обработки"""
        self.source['csv'] = kwargs.get('csv', None)
        self.source['excel'] = kwargs.get('excel', None)

    def get_data(self):
        """Возвращает данные для вставки в шаблон"""
        self.context['items'] = self.create_content()
        return self.context

    def create_content(self):
        """Получает данные для рендеринга шаблона"""
        raise NotInheritedMethodTypeError('Метод %s должен быть переопределен, в дочернем классе.' % self.set_data.__name__)


class BaseExcelModel:
    """Определяет модель файла Excel"""

    type = None # тип генерируемой документации
    result = None # путь куда сохранить сгенерированный документ
    template = None # путь к файлу шаблону
    sheets_data = [] # данные от страниц
    sheets = {} # страницы привязанные к этому файлу
    source = {} # данные для обработки

    def __init__(self, type, path):
        """Создает объект файла"""
        if type not in DOCUMENT_TYPE:
            raise NotAllowedDocumentationTypeError('Не разрешенный тип документации `%s`' % type)
        self.type = type
        self.template = path
        self.writer = BookWriter(path)

    def set_data(self, **kwargs):
        """Полчаем данные для обработки и передаем их страницм"""
        self.source['csv'] = kwargs.get('csv', None)
        self.source['excel'] = kwargs.get('excel', None)
        for sheet in self.sheets.values():
            sheet.set_data(csv=self.source['csv'], excel=self.source['excel'])

    def set_sheet(self, worker, name, sheet_index):
        """Устанавливает класс для обработки страницы с ее уникальной логикой"""
        self.sheets[name] = worker(self.type, name, sheet_index)

    def _get_data(self):
        """Возвращает данные от каждой из страниц"""
        self.sheets_data = [sheet.get_data() for sheet in self.sheets.values()]
        return self.sheets_data

    def save(self, path):
        """Получает данные из всех страниц и сохраняет файл"""
        self.writer.render_sheets(payloads=self._get_data())
        self.writer.save(path)
        self.result = path
        return path



