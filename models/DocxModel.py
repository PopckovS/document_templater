from models.BaseDocxModels import BaseDocxModel
from utils.utils import get_title_from_registration


class NoteDocx(BaseDocxModel):
    """Модель данных для документа 'Пояснительная записка' подходит для типов `gs` и `ngs`"""

    def _create_context(self):
        """Создает контекст данных для заполнения шаблона документа"""
        items, registration_numbers, summ = [], [], 0

        # Проходимся по данным из CSV и Docx, на их основе создаем контент
        for csv_row in self.data['csv']:
            for ex_row in self.data['excel']:

                # находим совпадение по кадастровым номерам
                if csv_row['Кадастровый номер'] == ex_row['Кадастровый номер ']:
                    data = {}
                    for key, val in self.source['csv'].items():
                        data[key] = csv_row[val]
                    for key, val in self.source['excel'].items():
                        data[key] = ex_row[val]

                    data = get_title_from_registration(data)

                    items.append(data)
                    registration_numbers.append(csv_row['Кадастровый номер'])
                    summ += int(csv_row['Площадь, кв. м'])
                    data = {}

        result = {'items': items, 'summ': summ, 'numbers': ', '.join(registration_numbers)}
        return result


class PetitionRemovalDocx(BaseDocxModel):
    """Модель данных для документа 'Ходатайство об изъятии', для документов типа `removal`"""

    def _create_context(self):
        """Создает контекст данных для заполнения шаблона документа"""
        registration_numbers = []
        for csv_row in self.data['csv']:
            registration_numbers.append(csv_row['Кадастровый номер'])
        return {'registration_numbers': ', '.join(registration_numbers)}


class AvtodorDocx(BaseDocxModel):
    """Модель данных для документа 'Шаблон КТ-ГК Автодор', для документов типа `removal`"""

    def _create_context(self):
        """Создает контекст данных для заполнения шаблона документа"""
        items = []

        # Проходимся по данным из CSV и Docx, на их основе создаем контент
        for csv_row in self.data['csv']:
            for ex_row in self.data['excel']:

                # находим совпадение по кадастровым номерам
                if csv_row['Кадастровый номер'] == ex_row['Кадастровый номер существующего ЗУ/КК']:
                    data = {}
                    for key, val in self.source['csv'].items():
                        data[key] = csv_row[val]
                    for key, val in self.source['excel'].items():
                        # "Выполнение договора" замена Q на N если Q пустой
                        if val == 'Примечание' and not ex_row[val]:
                            data[key] = ex_row['Площадь по проекту межевания, кв. м']
                        else:
                            data[key] = ex_row[val]

                        data = get_title_from_registration(data)

                    items.append(data)

        result = {'items': items}
        return result


