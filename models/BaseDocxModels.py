import copy

from docxtpl import DocxTemplate, Listing
from exceptions import NotInheritedMethodTypeError, NotAllowedDocumentationTypeError
from names import DOCUMENT_TYPE, DOCX_TEMPLATE


class BaseDocxModel:
    """Определяет модель файла Docx"""

    type = None # тип документа
    name = None # название документа
    template = None # путь к шаблону
    source = None # поля для шаблона которые требуется заполнить
    result = None # путь к готовому файлу
    data = {} # результат обработки, данные для вставки в шаблон страницы

    def __init__(self, type, name, path):
        if type not in DOCUMENT_TYPE or not DOCX_TEMPLATE[type][name]:
            raise NotAllowedDocumentationTypeError('Не разрешенный тип документации `%s`' % type)
        self.fields = copy.deepcopy(DOCX_TEMPLATE[type][name])
        self.writer = DocxTemplate(path)
        self.type = type
        self.name = name
        self.template = path

    def save(self, path):
        """Генерирует контекст, рендерит файл docx и сохраняет его"""
        self.result = path
        self.data = self._create_context()
        self.writer.render(self.data)
        self.writer.save(path)

    def set_data(self, **kwargs):
        """Получает данные для рендеринга шаблона"""
        self.source['csv'] = kwargs.get('csv', None)
        self.source['excel'] = kwargs.get('excel', None)

    def _create_context(self):
        """Создает контекст данных для заполнения шаблона документа"""
        raise NotInheritedMethodTypeError('Метод %s должен быть переопределен, в дочернем классе.' % self.create_context.__name__)










