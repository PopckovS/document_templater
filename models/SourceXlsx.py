from openpyxl_templates import TemplatedWorkbook, TemplatedWorksheet


"""
Описывает модель файла Excel который будет выступать
в качестве источника данных для создания файла Excel Перечня 
"""


class ListSheet(TemplatedWorksheet):
    """Модель xlsx страницы для файла: ВЫПОЛНЕНИЕ ДОГОВОРА"""

    def write(self, data):
        """Превращает полученный список в строку xlsx"""
        worksheet = self.worksheet
        for row in data:
            worksheet.append(row)

    def read(self):
        """Возвращает значения строк страницы xlsx в виде списка со списками"""
        worksheet = self.worksheet
        data = []
        for row in worksheet.rows:
            data.append([elem.value for elem in row])
        return data

    def read_dict(self):
        """СЛОЖНАЯ ЛОГИКА"""
        worksheet = self.worksheet
        data, title, first = [], [], True

        # получаем первую строку файла и делаем из нее заголовки
        for row in worksheet.rows:
            if not first:
                break
            title = [elem.value for elem in row]
            first = False

        # map функция, объединяет заголовок и значение в кортеж
        def map_concat(row):
            result = dict(zip(title, row))
            return result

        # превращаем строки в списки, один список одна строка из, Excel и удаляем перенос строки
        for row in worksheet.rows:
            data.append([elem.value for elem in row])
        result = list(map(map_concat, data))
        return result

    def read_obj(self):
        """Возвращает объекты строк страницы xlsx в виде списка со списками"""
        worksheet = self.worksheet
        data = []
        for row in worksheet.rows:
            data.append([elem for elem in row])
        return data


class SourceWorkbook(TemplatedWorkbook):
    """Модель файла xlsx"""
    Sheet1 = ListSheet(sheetname="выполнение")


def get_data_from_excel(path):
    workbook = SourceWorkbook(file=path)
    return workbook.Sheet1.read_dict()

