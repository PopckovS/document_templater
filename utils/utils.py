import csv
import glob
import logging
import os

from rrd_xml_owner.exceptions import NotImplementedTypeError, NotImplementedValueError
from rrd_xml_owner.owner import OwnerXML
from names import HEADER_LIST, HEADER
from rrd_xml_analyzer.analyzer import analyze

"""
Предоставляет функцию для конвертации xml файлов в файла csv.
При конвертации вызывает вспомогательные функции для обработки 
данных в зависимости от типа генерируемых документов.
"""

logger = logging.getLogger(__name__)


class ITT(csv.Dialect):
    """Новый диалект для валидной выгрузки."""
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\n'
    quoting = csv.QUOTE_ALL


csv.register_dialect("itt", ITT)


def convert_xml2csv(path, analyze=False):
    """Конвертирует все найденные xml файлы в csv по указанному пути"""
    try:
        xml_files = glob.glob(f'{path}/*.xml')
        csv_files = [xml2csv(path, analyze=analyze,) for path in xml_files]
    except Exception as e:
        logger.exception(e)
    else:
        logger.debug('Все xml файлы успешно конвертированы в csv по пути : %s', os.path.basename(path))
    return csv_files


def xml2csv(path, **kwargs):
    """
    Функция извлекает данные о владельце из xml-документа и сохраняет в csv формате.
    Используется чтобы извлекать данные только о владельце.

    :param str path: полный путь к xml-документу
    :param dict[str] kwargs: дополнительные именованные аргументы
    :param pre_work[bool]: обработать ли поля специальными функциями перед сохранением в CSV
    :key str type_xml: тип xml-документа
    :key str version_xml: версия xml-документа
    :return: список путей до преобразованных документов
    :rtype: list[str]
    """
    path_output = os.path.extsep.join([os.path.splitext(path)[0], 'csv'])
    result = path_output

    # получаем данные о владельце
    with open(path_output, mode='w') as f:
        output = csv.DictWriter(f, fieldnames=[HEADER[key] for key in HEADER_LIST], dialect='itt')
        output.writeheader()
        Owner = OwnerXML()
        try:
            for owner in Owner.get_all_owners(path,
                                              type_xml=kwargs.get('type_xml', None),
                                              version_xml=kwargs.get('version_xml', None)
                                              ):
                # получаем из xml информацию о дате
                data = analyze(path,
                               type_xml=kwargs.get('type_xml', None),
                               version_xml=kwargs.get('version_xml', None)
                               )
                owner['date'] = data['date_formation']
                owner['reg_number'] = data['registration_number']

                output.writerow({HEADER[key]: owner.get(key, None) for key in HEADER_LIST})

        except (NotImplementedTypeError, NotImplementedValueError) as e:
            logger.error(str(e))
        except Exception as e:
            logger.exception(e)
        else:
            logger.debug('Обработка файла выполнена успешно: %s', os.path.basename(path))
        finally:
            del Owner
    return result


def get_dict_from_csv(csv_files, **kwargs):
    """Проходит по csv файлам и возвращает из них данные в виде списка со словарями"""
    data = []

    # проходимся по каждому из csv файлов
    for path in csv_files:
        with open(path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            tmp_data = {}
            # сохраняем данные из csv файла
            for row in reader:
                for key, value in row.items():
                    tmp_data[key] = value

            data.append(tmp_data)

    # TODO перенести в другое место
    # добавляем номер строки для каждого списка
    for index in range(len(data)):
        data[index]['counter'] = index+1

    return data


def get_title_from_registration(data):
    """Получает только название из типа регистрации"""
    if 'registration' in data:
        data['registration'] = data['registration'].split(',')[0] \
            if data['registration'] and len(data['registration']) > 0 else data['registration']
    return data
