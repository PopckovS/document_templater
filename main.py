from models.BaseExcelModels import BaseExcelModel
from models.ExcelModel import RemovalSheet, ListSheet, SummaryStatementSheet
from models.DocxModel import NoteDocx, PetitionRemovalDocx, AvtodorDocx
from models.SourceXlsx import get_data_from_excel
from utils.utils import convert_xml2csv, get_dict_from_csv

# Настрйоки для теста
excel_source = 'document/data/ВЫПОЛНЕНИЕ ДОГОВОРА.xlsx'

gs = {
    # пути к файлам docx
    'template_note': 'document/template_1/docx/Шаблон Пояснительная записка.docx',
    'result_note': 'document/template_1/docx/Пояснительная записка.docx',

    # пути к файлам xlsx
    'template_scroll': 'document/template_1/xlsx/Шаблон перечней.xlsx',
    'result_scroll': 'document/template_1/xlsx/Перечень.xlsx',

    # пути к файлам xml
    'path_xml': 'document/template_1/xml2csv/'
}
ngs = {
    # пути к файлам docx
    'template_note': 'document/template_2/docx/Шаблон Пояснительная записка.docx',
    'result_note': 'document/template_2/docx/Пояснительная записка.docx',

    # пути к файлам xlsx
    'template_scroll': 'document/template_2/xlsx/Шаблон перечней.xlsx',
    'result_scroll': 'document/template_2/xlsx/Перечень.xlsx',

    # пути к файлам xml
    'path_xml': 'document/template_2/xml2csv/'
}
removal = {
    # шаблоны для файлов docx
    'template_avtodor': 'document/template_3/docx/Шаблон КТ-ГК_Автодор__на изъятие_собст (32-63).docx',
    'template_note': 'document/template_3/docx/Шаблон Пояснительная записка к изъятию.docx',
    'template_petition': 'document/template_3/docx/Шаблон Ходатайство об изъятии.docx',

    # сгенерированные файлы docx
    'result_avtodor': 'document/template_3/docx/КТ-ГК_Автодор__на изъятие_собст (32-63).docx',
    'result_note': 'document/template_3/docx/Пояснительная записка к изъятию.docx',
    'result_petition': 'document/template_3/docx/Ходатайство об изъятии.docx',

    # пути к файлам xlsx
    'template_scroll': 'document/template_3/xlsx/Шаблон перечень изъятия.xlsx',
    'result_scroll': 'document/template_3/xlsx/Перечень изъятия.xlsx',

    # пути к файлам xml
    'path_xml': 'document/template_3/xml2csv/'
}


def func1():
    """ Набор документов `gs` """
    # получаем данные
    paths = convert_xml2csv(gs['path_xml'])
    csv_dict = get_dict_from_csv(paths)
    excel_dict = get_data_from_excel(excel_source)

    # создание файла Перечень.xlsx
    excel = BaseExcelModel('gs', gs['template_scroll'])
    excel.set_sheet(worker=ListSheet, name='Перечень', sheet_index=0)
    excel.set_sheet(worker=SummaryStatementSheet, name='Сводная ведомость', sheet_index=1)
    excel.set_data(csv=csv_dict, excel=excel_dict)
    excel.save('result/Перечень.xlsx')

    # создания файла Пояснительная записка.docx
    docx = NoteDocx('gs', 'Пояснительная записка', gs['template_note'])
    docx.set_data(csv_dict, excel_dict)
    docx.save('result/Пояснительная записка.docx')


def func2():
    """ Набор документов `ngs` """
    # получаем данные
    paths = convert_xml2csv(ngs['path_xml'])
    csv_dict = get_dict_from_csv(paths)
    excel_dict = get_data_from_excel(excel_source)

    # создание файла Перечень.xlsx
    excel = BaseExcelModel('ngs', ngs['template_scroll'])
    excel.set_sheet(worker=ListSheet, name='Перечень', sheet_index=0)
    excel.set_sheet(worker=SummaryStatementSheet, name='Сводная ведомость', sheet_index=1)
    excel.set_data(csv=csv_dict, excel=excel_dict)
    excel.save('result/Перечень.xlsx')

    # создания файла Пояснительная записка.docx
    docx = NoteDocx('ngs', 'Пояснительная записка', ngs['template_note'])
    docx.set_data(csv_dict, excel_dict)
    docx.save('result/Пояснительная записка.docx')


def func3():
    """ Набор документов `removal` """

    # получаем данные
    paths = convert_xml2csv(removal['path_xml'])
    csv_dict = get_dict_from_csv(paths)
    excel_dict = get_data_from_excel(excel_source)

    # создаем документ Перечень.xlsx
    excel = BaseExcelModel(type='removal', path=removal['template_scroll'])
    excel.set_sheet(worker=RemovalSheet, name='Изъятие', sheet_index=0)
    excel.set_data(csv_dict, excel_dict)
    excel.save('result/Перечень.xlsx')

    # создаем документ Изъятие.docx
    petition = PetitionRemovalDocx(type='removal', name='Изъятие', path=removal['template_petition'])
    petition.set_data(csv_dict)
    petition.save('result/Изъятие.docx')

    # создаем документ Пояснительная записка.docx
    docx = AvtodorDocx(type='removal', name='Пояснительная записка', path=removal['template_note'])
    docx.set_data(csv_dict, excel_dict)
    docx.save('result/Пояснительная записка.docx')

    # создаем документ Автодор.docx
    petition = AvtodorDocx(type='removal', name='Автодор', path=removal['template_avtodor'])
    petition.set_data(csv_dict, excel_dict)
    petition.save('result/Автодор.docx')


if __name__ == '__main__':
    func1()
    # func2()
    # func3()

