# поля для извлечения из XML
HEADER_LIST = [
    'owners',
    'address',
    'category',
    'utilization_bydoc',
    'registration_number',
    'area',
    'registration',
    'date',
    'reg_number'
]

# данные из XML в CSV под этими заголовками
HEADER = {
    'owners': 'Субъект права',
    'address': 'Местоположение',
    'category': 'Категория земель',
    'utilization_bydoc': 'Вид разрешенного использования',
    'registration_number': 'Кадастровый номер',
    'area': 'Площадь, кв. м',
    'registration': 'Вид права',
    'date': 'Дата',
    'reg_number': 'Номер регистрации',
}


# типы наборов документации
DOCUMENT_TYPE = [
    'gs',
    'ngs',
    'removal'
]

# Описываем источники данных для файлов Excel
EXCEL_TEMPLATE = {

    # Первый тип документов Excel
    'gs': {
        'Перечень': {
            'csv': {
                'counter': 'counter',
                'owners': 'Субъект права',
                'address': 'Местоположение',
                'category': 'Категория земель',
                'utilization_bydoc': 'Вид разрешенного использования',
                'registration_number': 'Кадастровый номер',
                'area': 'Площадь, кв. м',
                'registration': 'Вид права'
            },
            'excel': None
        },
        'Сводная ведомость': {
            'csv': {
                'counter': 'counter',
                'address': 'Местоположение',
                'registration_number': 'Кадастровый номер',
                'area': 'Площадь, кв. м'
            },
            'excel': {
                'mezh': 'Схема проекта межевания',
                'schema_number': 'Условный номер участка на схеме',
                'area_mezh': 'Площадь по проекту межевания, кв. м'
            }
        }
    },

    # Второй тип документов Excel
    'ngs': {
        'Перечень': {
            'csv': {
                'counter': 'counter',
                'address': 'Местоположение',
                'category': 'Категория земель',
                'utilization_bydoc': 'Вид разрешенного использования',
                'registration_number': 'Кадастровый номер',
                'area': 'Площадь, кв. м',
                'registration': 'Вид права'
            },
            'excel': None
        },
        'Сводная ведомость': {
            'csv': {
                'counter': 'counter',
                'address': 'Местоположение',
                'registration_number': 'Кадастровый номер',
                'area': 'Площадь, кв. м'
            },
            'excel': {
                'mezh': 'Схема проекта межевания',
                'schema_number': 'Условный номер участка на схеме',
                'area_mezh': 'Площадь по проекту межевания, кв. м'
            }
        }
    },

    # Третий тип документов Excel
    'removal': {
        'Изъятие': {
            'csv': {
                'counter': 'counter',
                 'owners': 'Субъект права',
                'registration': 'Вид права',

                # константа = `Земельный участок`
                # 'naming': 'Наименование объекта недвижимости',

                'address': 'Местоположение',
                'registration_number': 'Кадастровый номер',
                'area': 'Площадь, кв. м',
                'reg_number': 'Номер регистрации',
                'date': 'Дата'
            },
            'excel': {
                # столбец H из выполненеи договора
                'area_removal': 'Исходный ВРИ образуемого/изменяемого ЗУ'
            }
        },
    },
}

# Описываем источники данных для файлов Docx
DOCX_TEMPLATE = {

    # Первый тип документов Docx
    'gs': {
        'Пояснительная записка': {
            'csv': {
                'registration_number': 'Кадастровый номер',
                'registration': 'Вид права',
                'area': 'Площадь, кв. м',
                'owner': 'Субъект права',
            },
            'excel': {
                'registration_number_zu': 'Кадастровый номер существующего ЗУ/КК',
                'area_zu': 'Площадь существующего ЗУ, кв. м',
                'area_mezh': 'Площадь по проекту межевания, кв. м',
                'number': 'Условный номер участка на схеме',
                'area_too': 'Площадь по проекту межевания, кв. м',
            }
        }
    },

    # Второй тип документов Docx
    'ngs': {
        'Пояснительная записка': {
            'csv': {
                'registration_number': 'Кадастровый номер',
                'registration': 'Вид права',
                'area': 'Площадь, кв. м',
                'owner': 'Субъект права',
            },
            'excel': {
                'registration_number_zu': 'Кадастровый номер существующего ЗУ/КК',
                'area_zu': 'Площадь существующего ЗУ, кв. м',
                'area_mezh': 'Площадь по проекту межевания, кв. м',
                'number': 'Условный номер участка на схеме',
                'area_too': 'Площадь по проекту межевания, кв. м',
            }
        }
    },

    # Третий тип документов Docx
    'removal': {
        'Пояснительная записка': {
            'csv': {
                'registration_number': 'Кадастровый номер',
                'registration': 'Вид права',
                'area': 'Площадь, кв. м',
                'owner': 'Субъект права',
            },
            'excel': {
                'number': 'Условный номер участка на схеме',
                'get_area': 'Примечание'
            }
        },
        'Автодор': {
            'csv': {
                'registration_number': 'Кадастровый номер',
                'date': 'Дата'
            },
            'excel': {
                'number': 'Условный номер участка на схеме',
                'get_area': 'Примечание'
            }
        },
        'Изъятие': {
            'csv': {
                'registration_number'
            }
        },
    }
}







